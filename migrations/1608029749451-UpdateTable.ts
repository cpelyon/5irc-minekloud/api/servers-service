import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateTable1608029749451 implements MigrationInterface {
    name = 'UpdateTable1608029749451'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server"
            ALTER COLUMN "maxAvailableRAM" TYPE numeric(2)
        `);
        await queryRunner.query(`
            COMMENT ON COLUMN "server"."maxAvailableRAM" IS NULL
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            COMMENT ON COLUMN "server"."maxAvailableRAM" IS NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "server"
            ALTER COLUMN "maxAvailableRAM" TYPE numeric
        `);
    }

}
