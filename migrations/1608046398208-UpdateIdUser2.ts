import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateIdUser21608046398208 implements MigrationInterface {
    name = 'UpdateIdUser21608046398208'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server"
            ADD "idUser" integer NOT NULL
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server" DROP COLUMN "idUser"
        `);
    }

}
