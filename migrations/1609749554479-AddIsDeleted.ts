import {MigrationInterface, QueryRunner} from "typeorm";

export class AddIsDeleted1609749554479 implements MigrationInterface {
    name = 'AddIsDeleted1609749554479'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server"
            ADD "isDeleted" boolean NOT NULL DEFAULT false
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server" DROP COLUMN "isDeleted"
        `);
    }

}
