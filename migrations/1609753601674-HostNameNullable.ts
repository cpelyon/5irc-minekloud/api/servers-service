import {MigrationInterface, QueryRunner} from "typeorm";

export class HostNameNullable1609753601674 implements MigrationInterface {
    name = 'HostNameNullable1609753601674'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server"
            ALTER COLUMN "hostname" DROP NOT NULL
        `);
        await queryRunner.query(`
            COMMENT ON COLUMN "server"."hostname" IS NULL
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            COMMENT ON COLUMN "server"."hostname" IS NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "server"
            ALTER COLUMN "hostname"
            SET NOT NULL
        `);
    }

}
