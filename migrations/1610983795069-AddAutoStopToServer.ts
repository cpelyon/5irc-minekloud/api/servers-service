import {MigrationInterface, QueryRunner} from "typeorm";

export class AddAutoStopToServer1610983795069 implements MigrationInterface {
    name = 'AddAutoStopToServer1610983795069'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server"
            ADD "autoStop" boolean DEFAULT false
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server" DROP COLUMN "autoStop"
        `);
    }

}
