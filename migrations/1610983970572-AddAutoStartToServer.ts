import {MigrationInterface, QueryRunner} from "typeorm";

export class AddAutoStartToServer1610983970572 implements MigrationInterface {
    name = 'AddAutoStartToServer1610983970572'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server"
            ADD "autoStart" boolean DEFAULT false
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server" DROP COLUMN "autoStart"
        `);
    }

}
