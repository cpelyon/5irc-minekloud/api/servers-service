import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {cors: true});
  const config: ConfigService = app.get(ConfigService);

  app.setGlobalPrefix('/api');

  //Set API docs paramaters
  const options = new DocumentBuilder()
    .setTitle('Servers service')
    .setDescription('The servers service API description')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/docs/servers', app, document);


  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    transform: true
  }));

  await app.listen(config.get<number>('APP_PORT'));
}
bootstrap();
