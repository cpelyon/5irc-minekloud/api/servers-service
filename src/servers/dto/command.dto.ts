import { ApiProperty } from '@nestjs/swagger'
import { IsString } from 'class-validator'
export class CommandDto {
  @IsString()
  @ApiProperty()
  command: string
}
