import {IsNotEmpty, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateServerDto {
    @ApiProperty()
    @IsNotEmpty({})
    idUser: number;

    @ApiProperty()
    @IsNotEmpty({})
    name: string;

    @ApiProperty()
    @IsNotEmpty({})
    hostname: string;

    @ApiProperty()
    @IsNotEmpty({})
    maxAvailableRAM: number;

    @ApiProperty({default: false})
    @IsOptional()
    autoStop?: boolean
  
    @ApiProperty({default: false})
    @IsOptional()
    autoStart?: boolean
}