import { ApiProperty } from '@nestjs/swagger'
import { IsString } from 'class-validator'
export class PlayerDto {
  @IsString()
  @ApiProperty()
  username: string
}