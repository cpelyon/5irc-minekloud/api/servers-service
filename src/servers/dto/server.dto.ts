import { ApiProperty } from '@nestjs/swagger'
import { ServerStatus } from '../models/serverStatus'
import { PlayerDto } from './player.dto'

export class ServerDto {
  @ApiProperty()
  id: number

  @ApiProperty()
  idUser : number

  @ApiProperty()
  name: string

  @ApiProperty()
  hostname: string

  @ApiProperty()
  status: ServerStatus

  @ApiProperty()
  maxAvailableRAM: number

  @ApiProperty()
  deletedAt: Date;

  @ApiProperty()
  autoStop: boolean

  @ApiProperty()
  autoStart: boolean

  @ApiProperty()
  players: PlayerDto[]
}