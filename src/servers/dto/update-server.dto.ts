import {ServerStatus} from '../models/serverStatus'
import { ApiProperty } from '@nestjs/swagger';
import {IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateServerDto {

    @ApiProperty()
    @IsOptional()
    @IsString()
    name: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    hostname: string;

    @ApiProperty()
    @IsOptional()
    @IsNotEmpty({})
    status: ServerStatus;

    @ApiProperty()
    @IsOptional()
    @IsNumber()
    maxAvailableRAM: number;

    @ApiProperty()
    @IsOptional()
    @IsBoolean()
    autoStart: boolean;

    @ApiProperty()
    @IsOptional()
    @IsBoolean()
    autoStop: boolean;
}