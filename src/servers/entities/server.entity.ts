import { Column, DeleteDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import {ServerStatus} from '../models/serverStatus'

@Entity()
export class Server {

  @ApiProperty({required: false})
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  idUser : number;

  @ApiProperty()
  @Column()
  name: string;

  @ApiProperty()
  @Column({unique: true, nullable: true})
  hostname: string;

  @ApiProperty()
  @Column({type: 'enum',enum: ServerStatus, default: ServerStatus.OFF})
  status: ServerStatus;

  @ApiProperty()
  @Column({type: 'decimal', precision: 3, scale: 1 })
  maxAvailableRAM: number;

  @ApiProperty()
  @DeleteDateColumn()
  deletedAt: Date;

  @ApiProperty()
  @Column({default: false, nullable: true})
  autoStop: boolean

  @ApiProperty()
  @Column({default: false, nullable: true})
  autoStart: boolean
}
