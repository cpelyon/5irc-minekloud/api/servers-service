import { Get, ParseIntPipe, 
  Body ,Param, Post, Delete, HttpCode, HttpStatus, Controller} from '@nestjs/common';
import { ApiBody, ApiOperation, 
  ApiOkResponse } from '@nestjs/swagger';

import { PlayerDto } from './dto/player.dto'
import { PlayerService } from './services/player.service';

@Controller()
export class PlayerController {
  constructor(private readonly playerService: PlayerService) {}

  //Add online players to a server
  //Return status 204
  @ApiBody({type: PlayerDto, isArray: true})
  @ApiOperation({ summary: 'Add online players to server' })
  @Post('servers/:id/players')
  @HttpCode(HttpStatus.NO_CONTENT)
  async addPlayersToServer(@Param('id', ParseIntPipe) id: number, @Body() playersDto: PlayerDto[]){
    playersDto.forEach(player => {
      this.playerService.addConnectedPlayer(player, id)
    })
  }

  //Get online players on server
  //Return player dto object
  @ApiOperation({ summary: 'Get connected players by server ID'})
  @ApiOkResponse({description:'Return player object',type: PlayerDto})
  @Get('servers/:id/players')
  async findPlayersConnectedByServer(@Param('id', ParseIntPipe) id: number): Promise<PlayerDto[]> {
    return this.playerService.getPlayersByServer(id)
  }

  //Delete (disconnect) an specific player (selected by username) from a server
  //Return status 204
  @ApiOperation({ summary: 'Delete online player by username and by server ID' })
  @Delete('servers/:id/players/:username')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(@Param('id', ParseIntPipe) id: number, @Param('username') username: string) {
    this.playerService.removeConnectedPlayer(username, id)
  }
}