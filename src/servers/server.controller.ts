import { Controller, Get, ParseIntPipe, NotFoundException, 
    Body ,Param, Post, Patch, Delete, Query, HttpException } from '@nestjs/common';
import { ServerService } from './services/server.service';
import {Server} from './entities/server.entity';
import { ApiBody, ApiConflictResponse, ApiOperation, ApiNotFoundResponse, 
    ApiOkResponse, ApiTags, ApiServiceUnavailableResponse, ApiBadRequestResponse, ApiQuery } from '@nestjs/swagger';
import {CreateServerDto} from './dto/create-server.dto';
import {UpdateServerDto} from './dto/update-server.dto';

@ApiTags('Servers')
@Controller('servers')
export class ServerController {
  constructor(private readonly serverService: ServerService) {}

  //Get servers (with filters)
  //Return list of servers objects or NotFoundException if nothing found
  @ApiOperation({ summary: 'Get servers'})
  @ApiOkResponse({description:'Return servers objects',type: [Server]})
  @ApiNotFoundResponse({description:'No server found'})
  @ApiQuery({name:'maxAvailableRam',required:false})
  @ApiQuery({name:'idUser',required:false})
  @ApiQuery({name:'hostname',required:false})
  @ApiQuery({name:'name',required:false})
  @Get()
  async find(@Query() query) {
    return await this.serverService.find(query);
  }
  

  //Get specific server by ID
  //Return server object if found, otherwise expection not found
  @ApiOperation({ summary: 'Get server by ID'})
  @ApiOkResponse({description:'Return server object',type: Server})
  @ApiNotFoundResponse({description: 'Server not foud with the ID specified'})
  @Get(':id')
  async findById(@Param('id') id: number){
    let server = await this.serverService.findById(id);
    if (!server) throw new NotFoundException;
    return server;
  }

  //Create a new server
  //Return status 201 or ConflitException
  @ApiBody({type: CreateServerDto})
  @ApiOperation({ summary: 'Create new server for a user' })
  @ApiOkResponse({description:'Server was successfully created'})
  @ApiConflictResponse({description:'Cannot create server, maybe duplicate hostname ?'})
  @ApiServiceUnavailableResponse({description:'hyperkube unreachable'})
  @Post()
  async create(@Body() createServerDto: CreateServerDto) {
    let exception = await this.serverService.create(createServerDto);
    if (exception) throw exception;
  } 

  //Update some server's informations
  //Return server object or NotFoundException (404)
  @ApiBody({type: UpdateServerDto})
  @ApiOperation({ summary: 'Update server informations' })
  @ApiOkResponse({description:'Server was successfully updated',type: Server})
  @ApiNotFoundResponse({description:'Server id doesn\'t exists !'})
  @ApiServiceUnavailableResponse({description:'hyperkube unreachable'})
  @ApiBadRequestResponse({description: 'Wrong status given'})
  @Patch(':id')
  async update(@Param('id', ParseIntPipe) id: number, @Body() updateServerDto: UpdateServerDto) {
    const result = await this.serverService.update(id,updateServerDto);
    if (result instanceof HttpException) throw result;
    //const server = await this.serverService.findById(result.id) 
    return result;
  }

  //Delete an specific server
  //Return status 200 or NotFoundException
  @ApiOperation({ summary: 'Delete server' })
  @ApiOkResponse({description:'Server was successfully deleted'})
  @ApiNotFoundResponse({description:'Server id doesn\'t exists !'})
  @ApiServiceUnavailableResponse({description:'hyperkube unreachable'})
  @Delete(':id')
  async delete(@Param('id', ParseIntPipe) id: number) {
    let exception = await this.serverService.delete(id);
    if (exception) throw exception;
  }
}
