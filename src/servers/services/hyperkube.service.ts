import { Controller, HttpService, Injectable} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class HyperkubeService {

    constructor(private httpService : HttpService, private configService: ConfigService) {}
    
    async notifyHyperkube(serverId: string) : Promise<boolean> {
        const baseUrl = this.configService.get('HYPERKUBE_URL')
        const result = await this.httpService.post(`${baseUrl}/notifyserverchange/${serverId}`)
            .toPromise()
            .then(() => true)
            .catch((e) => {
                console.log('Request to HyperKube failed:', typeof e);
                return false;
            });
        return result;
    }

    async sendCommand(serverId: string, command: string) : Promise<boolean> {
        const baseUrl = this.configService.get('HYPERKUBE_URL')
        const result = await this.httpService.post(`${baseUrl}/sendcommand/${serverId}`, { command })
            .toPromise()
            .then(() => true)
            .catch((e) => {
                console.log('Request to HyperKube failed:', typeof e);
                return false;
            });
        return result;
    }
}
