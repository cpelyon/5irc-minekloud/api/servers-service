import { Injectable } from '@nestjs/common'
import { PlayerDto } from '../dto/player.dto'

@Injectable()
export class PlayerService {
    private serversPlayersMap: Map<number, PlayerDto[]> = new Map()
   
    removeConnectedPlayer(username: string, serverId: number) {
      const playersByServer = this.serversPlayersMap.get(serverId)
      if(playersByServer) {
        console.log(`Player ${username} is now disconnected from server <${serverId}>`)
        const players = playersByServer.filter(player => player.username != username)
        this.serversPlayersMap.set(serverId, players)
      }
    }

    addConnectedPlayer(newPlayer: PlayerDto, serverId: number) {
      if (!this.serversPlayersMap.has(serverId)) {
        console.log(`Player ${newPlayer.username} is now connected to server <${serverId}>`)
        this.serversPlayersMap.set(serverId, [newPlayer])
        return
      }   

      const players = this.serversPlayersMap.get(serverId) as PlayerDto[]
      const playerExist = players.filter(player => player.username == newPlayer.username)
      if (playerExist.length != 0) {
        return
      }
      console.log(`Player ${newPlayer.username} is now connected to server <${serverId}>`)
      players.push(newPlayer)
    }

    getPlayersByServer(serverId: number): PlayerDto[] {
      if(this.serversPlayersMap.has(serverId)){
        return this.serversPlayersMap.get(serverId)
      }
      return []
    }
}
