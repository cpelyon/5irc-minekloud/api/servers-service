import { HttpService,Query, ConflictException, ServiceUnavailableException, NotFoundException} from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { Server } from '../entities/server.entity';
import { ServerService } from './server.service';
import {HyperkubeService} from './hyperkube.service'
import {ServerStatus} from '../models/serverStatus'
import { ConfigService } from '@nestjs/config';
import { CreateServerDto } from '../dto/create-server.dto';
import { UpdateServerDto } from '../dto/update-server.dto';
import { PlayerService } from './player.service';
import { ServerDto } from '../dto/server.dto';


const oneServer = new Server();
const serverArray = [
  new Server(),
  new Server(),
  new Server(),
];

//const oneCat = new Cat(testCat1, testBreed1, 4);

describe('ServerService', () => {
    let service: ServerService;
    let repository: Repository<Server>;
    let hyperkube: HyperkubeService;

    let server : Server = {id:1,hostname: "test",idUser:1,maxAvailableRAM:10,
        name:"test",status:ServerStatus.OFF,deletedAt:null, autoStart: false, autoStop: false};
    let serverDto : ServerDto = {id:1,hostname: "test",idUser:1,maxAvailableRAM:10,
    name:"test",status:ServerStatus.OFF,deletedAt:null, autoStart: false, autoStop: false,players: []};
    let server2 : Server = {id:2,hostname: "test2",
    idUser:1,maxAvailableRAM:10, name:"test2",status:ServerStatus.OFF,deletedAt:null, autoStart: false, autoStop: false};
    let listServers : Server[] = [server,server2];
    const mockHttpModule = jest.fn().mockReturnValue({});

    let createServer : CreateServerDto = {name: 'test3',hostname:'test5',idUser:1,maxAvailableRAM:2};
    let updateServer : UpdateServerDto = {name: 'test',hostname:'newHostname',maxAvailableRAM:2,status:ServerStatus.OFF,autoStart: true, autoStop: true};
    beforeEach(async () => {
      const mockRepository = {
        find: jest.fn(),
        findOne: jest.fn(),
        create: jest.fn(),
        save: jest.fn(),
        preload: jest.fn(),
        softDelete: jest.fn()
      }
  
      const module: TestingModule = await Test.createTestingModule({
        providers: [
          ServerService,
          {
            provide: getRepositoryToken(Server),
            useValue: mockRepository,
          }, HyperkubeService, ConfigService, HttpService, PlayerService
        ],
      })
      .overrideProvider(HttpService)
      .useValue(mockHttpModule)
      .compile();
  
      hyperkube = module.get<HyperkubeService>(HyperkubeService);
      service = module.get<ServerService>(ServerService);
      repository = module.get<Repository<Server>>(getRepositoryToken(Server));
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
    
    it('find with no query args shoud return list servers found by repository', async () => {
      jest.spyOn(repository, 'find').mockImplementation(async () => {return listServers});
      expect(await service.find(Query())).toMatchObject(listServers)
    });

    it('find with name query arg, shoud return one server found by repository', async () => {
      jest.spyOn(repository, 'find').mockImplementation(async () => {return [server]});
      expect(await service.find({name: server.name})).toMatchObject([server])
    });

    it('create with not existing hostname and hyperkube ok should return null', async () => {
      jest.spyOn(repository, 'findOne').mockImplementation(async () => {return null});
      jest.spyOn(repository, 'create').mockImplementation(() => server);
      jest.spyOn(repository, 'save').mockImplementation(async () => {return server});
      jest.spyOn(hyperkube, 'notifyHyperkube').mockImplementation(async () => {return true});
      expect(await service.create(createServer)).toBeNull();
    });

    it('create with existing hostname and hyperkube ok should return ConflitException', async () => {
      jest.spyOn(repository, 'findOne').mockImplementation(async () => {return server2});
      expect(await service.create(createServer)).toBeInstanceOf(ConflictException)
    });

    it('create with no existing hostname and hyperkube Nok should continue', async () => {
      jest.spyOn(repository, 'findOne').mockImplementation(async () => {return null});
      jest.spyOn(repository, 'create').mockImplementation(() => server);
      jest.spyOn(repository, 'save').mockImplementation(async () => {return server});
      jest.spyOn(hyperkube, 'notifyHyperkube').mockImplementation(async () => {return false});
      expect(await service.create(createServer)).toBeNull()
    });

    it('update with existing id and hyperkube ok should return updated server object', async () => {
      jest.spyOn(repository, 'preload').mockImplementation(async() => {server.hostname = updateServer.hostname;
        return server;});
        jest.spyOn(hyperkube, 'notifyHyperkube').mockImplementation(async () => {return true});
      jest.spyOn(repository, 'save').mockImplementation(async () => {return server});
      jest.spyOn(service, 'findById').mockImplementation(async () => {return serverDto});
      expect(await service.update(server.id,updateServer)).toMatchObject(serverDto);
    });

    it('update with no existing id and hyperkube ok should return updated server object', async () => {
      jest.spyOn(repository, 'preload').mockImplementation(async() => {return null});
      expect(await service.update(3,updateServer)).toBeInstanceOf(NotFoundException);
    });

    it('update with existing id and hyperkube Nok should return updated server object', async () => {
      jest.spyOn(repository, 'preload').mockImplementation(async() => {server.hostname = updateServer.hostname;
        return server;});
        jest.spyOn(hyperkube, 'notifyHyperkube').mockImplementation(async () => {return false});
        jest.spyOn(repository, 'save').mockImplementation(async () => {return server});
        jest.spyOn(service, 'findById').mockImplementation(async () => {return serverDto});
      expect(await service.update(server.id,updateServer)).toMatchObject(serverDto);
    });

    it('delete with existing id and hyperkube ok should return null', async () => {
      let deleteResult : UpdateResult = {affected:1,raw:null,generatedMaps:null};
      jest.spyOn(repository, 'softDelete').mockImplementation(async() => {return deleteResult});
      jest.spyOn(service, 'update').mockImplementation(async() => {return null});
      jest.spyOn(hyperkube, 'notifyHyperkube').mockImplementation(async () => {return true});
      expect(await service.delete(server.id)).toBeNull();
    });

    it('delete with not existing id and hyperkube ok should return null', async () => {
      let deleteResult : UpdateResult = {affected:0,raw:null,generatedMaps:null};
      jest.spyOn(service, 'update').mockImplementation(async() => {return null});
      jest.spyOn(repository, 'softDelete').mockImplementation(async() => {return deleteResult});
      expect(await service.delete(server.id)).toBeInstanceOf(NotFoundException)
    });

    it('delete with existing id and hyperkube Nok should return null', async () => {
      let deleteResult : UpdateResult = {affected:1,raw:null,generatedMaps:null};
      jest.spyOn(repository, 'softDelete').mockImplementation(async() => {return deleteResult});
      jest.spyOn(service, 'update').mockImplementation(async() => {return null});
      jest.spyOn(hyperkube, 'notifyHyperkube').mockImplementation(async () => {return false});
      expect(await service.delete(server.id)).toBeNull();
    });

});